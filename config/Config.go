package config

import (
	"log"

	"github.com/spf13/viper"
)

var (
	Config ServiceConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	//Environment variables
	viper.BindEnv("CURRENT_NAMESPACE")
}

type ServiceConfig struct {
	Server  ServerServiceConfig
	Traefik TraefikServiceConfig
	IPFS    IPFSServiceConfig
	Cluster ClusterServiceConfig
}

type ServerServiceConfig struct {
	ListenAddress string
	ApiPrefix     string
}

type TraefikServiceConfig struct {
	LoadBalancerService   string
	LoadBalancerNamespace string
}

type IPFSServiceConfig struct {
	Version   string
	SwarmPort int
	ApiPort   int
}

type ClusterServiceConfig struct {
	ClusterIdentityName string
	PrivateKeySecretKey string
	DefaultSecretName   string
}
