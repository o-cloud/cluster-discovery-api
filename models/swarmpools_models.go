package models

import (
	"context"
	"encoding/base64"
	"fmt"
	"strings"

	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/config"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const SwarmPoolsResource = "SwarmPools"

type ISwarmPools interface {
	GetSwarmPool(namespace string) (*v1alpha1.SwarmPool, error)
	ListSwarmPools() *[]v1alpha1.SwarmPool
	CreateSwarmPool(swarmpool *v1alpha1.SwarmPool, privateKey string) (*v1alpha1.SwarmPool, error)
	DeleteSwarmPool(namespace string) error
	UpdateSwarmPool(namespace string, swarmpool *v1alpha1.SwarmPool) (*v1alpha1.SwarmPool, error)
	GetSwarmPoolPrivateKey(namespace string, secretName string) string
	CreateSwarmPoolSecret(namespace string, secretName string, privateKey string)
	GetDefaultNetworkName() string
	GetNetworkState(namespace string) (api.NetworkState, error)
}

type SwarmPools struct {
	privateKeySecretKey string
	defaultSecretName   string
	KubeClient          *common.ExtendedClientset
}

func GetSwarmPools() ISwarmPools {
	return &SwarmPools{
		privateKeySecretKey: config.Config.Cluster.PrivateKeySecretKey,
		defaultSecretName:   config.Config.Cluster.DefaultSecretName,
		KubeClient:          common.GetKubeClient(),
	}
}

func (s *SwarmPools) GetSwarmPool(namespace string) (*v1alpha1.SwarmPool, error) {
	swarmpools := v1alpha1.SwarmPoolList{}
	err := s.KubeClient.PeerdiscoveryV1alpha1.Get().Namespace(namespace).Resource(SwarmPoolsResource).Do(context.TODO()).Into(&swarmpools)
	if err != nil {
		panic(err.Error())
	}
	swarmpoolsItems := swarmpools.Items
	if len(swarmpoolsItems) == 0 {
		err = fmt.Errorf("swarmpool %s not found", namespace)
		return &v1alpha1.SwarmPool{}, err
	}
	return &swarmpoolsItems[0], nil
}

func (s *SwarmPools) ListSwarmPools() *[]v1alpha1.SwarmPool {
	swarmpools := v1alpha1.SwarmPoolList{}
	err := s.KubeClient.PeerdiscoveryV1alpha1.Get().Resource(SwarmPoolsResource).Do(context.TODO()).Into(&swarmpools)
	if err != nil {
		panic(err.Error())
	}
	return &swarmpools.Items
}

func (s *SwarmPools) CreateSwarmPool(swarmpool *v1alpha1.SwarmPool, privateKey string) (*v1alpha1.SwarmPool, error) {
	// Namespace creation
	namespace := &v1.Namespace{}
	namespace.Name = swarmpool.Namespace
	_, err := s.KubeClient.Clientset.CoreV1().Namespaces().Create(context.TODO(), namespace, metav1.CreateOptions{})
	if err != nil && !errors.IsAlreadyExists(err) {
		panic(err.Error())
	}

	// Secret creation if private key submited
	if privateKey != "" {
		swarmKeySecretRef := swarmpool.Spec.Swarms[0].SwarmDefinition.SwarmKeySecretRef
		s.CreateSwarmPoolSecret(swarmpool.Namespace, swarmKeySecretRef, privateKey)
	}

	// Swarmpool creation
	result := v1alpha1.SwarmPool{}
	err = s.KubeClient.PeerdiscoveryV1alpha1.
		Post().
		Namespace(swarmpool.Namespace).
		Resource(SwarmPoolsResource).
		Body(swarmpool).
		Do(context.TODO()).
		Into(&result)

	if errors.IsAlreadyExists(err) {
		err = fmt.Errorf("swarmpool %s already exist", swarmpool.Namespace)
		return nil, err
	} else if err != nil {
		panic(err.Error())
	}

	return &result, err
}

func (s *SwarmPools) DeleteSwarmPool(namespace string) error {
	_, err := s.KubeClient.Clientset.CoreV1().Namespaces().Get(context.TODO(), namespace, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		err = fmt.Errorf("swarmpool %s not found", namespace)
		return err
	} else if err != nil {
		panic(err.Error())
	}

	err = s.KubeClient.Clientset.CoreV1().Namespaces().Delete(context.TODO(), namespace, metav1.DeleteOptions{})
	if err != nil {
		panic(err.Error())
	}

	return nil
}

func (s *SwarmPools) UpdateSwarmPool(namespace string, swarmpool *v1alpha1.SwarmPool) (*v1alpha1.SwarmPool, error) {
	return nil, nil
}

func (s *SwarmPools) CreateSwarmPoolSecret(namespace string, secretName string, privateKey string) {
	if privateKey == "" {
		err := fmt.Errorf("%s secret creation : privateKey can not be nil for network %s", secretName, namespace)
		panic(err)
	}

	err := s.KubeClient.Clientset.CoreV1().Secrets(namespace).Delete(context.TODO(), secretName, metav1.DeleteOptions{})
	if err != nil && !errors.IsNotFound(err) {
		panic(err.Error())
	}

	decodedPrivateKey, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		panic(err.Error())
	}

	secret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      secretName,
			Namespace: namespace,
		},
		Type: "Opaque",
		Data: map[string][]byte{s.privateKeySecretKey: []byte(decodedPrivateKey)},
	}
	_, err = s.KubeClient.Clientset.CoreV1().Secrets(namespace).Create(context.TODO(), secret, metav1.CreateOptions{})
	if err != nil {
		panic(err.Error())
	}
}

func (s *SwarmPools) GetSwarmPoolPrivateKey(namespace string, secretName string) string {
	if secretName == "" {
		secretName = s.defaultSecretName
	}

	secret, err := s.KubeClient.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		return ""
	} else if err != nil {
		panic(err.Error())
	}

	return base64.StdEncoding.EncodeToString(secret.Data[s.privateKeySecretKey])
}

func (s *SwarmPools) GetDefaultNetworkName() string {
	defaultSecret, err := s.KubeClient.Clientset.CoreV1().Secrets("").List(context.TODO(), metav1.ListOptions{
		FieldSelector: fmt.Sprintf("metadata.name=%s", s.defaultSecretName),
	})
	if err != nil {
		panic(err.Error())
	}

	if len(defaultSecret.Items) > 1 {
		for _, secret := range defaultSecret.Items {
			if !strings.Contains(secret.Namespace, "default-config") {
				return secret.Namespace
			}
		}
	}

	return ""
}

func (s *SwarmPools) GetNetworkState(namespace string) (api.NetworkState, error) {
	namespaceV1, err := s.KubeClient.Clientset.CoreV1().Namespaces().Get(context.TODO(), namespace, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		err = fmt.Errorf("swarmpool %s not found", namespace)
		return api.NetworkNotReady, err
	} else if err != nil {
		panic(err.Error())
	}

	if namespaceV1.Status.Phase == v1.NamespaceTerminating {
		return api.NetworkDeleting, nil
	}

	pods, err := s.KubeClient.Clientset.CoreV1().Pods(namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}

	for _, pod := range pods.Items {
		if len(pod.Status.ContainerStatuses) == 0 {
			continue
		}

		isIPFS := strings.Contains(pod.Status.ContainerStatuses[0].Image, "ipfs")
		isWaiting := pod.Status.ContainerStatuses[0].State.Waiting != nil
		isRunning := pod.Status.ContainerStatuses[0].State.Running != nil
		if isIPFS && isRunning {
			return api.NetworkReady, nil
		}
		if isIPFS && isWaiting {
			return api.NetworkPending, nil
		}
	}

	return api.NetworkNotReady, nil
}
