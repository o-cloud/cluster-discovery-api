package models

import (
	"context"
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/config"

	"k8s.io/apimachinery/pkg/api/errors"
)

const IdentitiesResource = "Identities"

type IIdentity interface {
	GetIdentity() (*v1alpha1.Identity, error)
	CreateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error)
	DeleteIdentity() error
	UpdateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error)
	CheckIdentitySwarmPool(network string) bool
	AddIdentitySwarmPool(network string) (*v1alpha1.Identity, error)
	DeleteIdentitySwarmPool(network string) (*v1alpha1.Identity, error)
}

type Identity struct {
	namespace           string
	clusterIdentityName string
	KubeClient          *common.ExtendedClientset
}

func GetIdentity() IIdentity {
	return &Identity{
		namespace:           viper.GetString("CURRENT_NAMESPACE"),
		clusterIdentityName: config.Config.Cluster.ClusterIdentityName,
		KubeClient:          common.GetKubeClient(),
	}
}

func (si *Identity) GetIdentity() (*v1alpha1.Identity, error) {
	identity := v1alpha1.Identity{}
	err := si.KubeClient.PeerdiscoveryV1alpha1.
		Get().
		Namespace(si.namespace).
		Name(si.clusterIdentityName).
		Resource(IdentitiesResource).
		Do(context.TODO()).
		Into(&identity)

	if errors.IsNotFound(err) {
		err = fmt.Errorf("identity not found")
		return nil, err
	} else if err != nil {
		panic(err.Error())
	}

	return &identity, nil
}

func (si *Identity) CreateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error) {
	result := v1alpha1.Identity{}
	identity.Name = si.clusterIdentityName
	err := si.KubeClient.PeerdiscoveryV1alpha1.
		Post().
		Namespace(si.namespace).
		Resource(IdentitiesResource).
		Body(identity).
		Do(context.TODO()).
		Into(&result)

	if errors.IsAlreadyExists(err) {
		err = fmt.Errorf("identity already exist")
		return nil, err
	} else if err != nil {
		panic(err.Error())
	}

	return &result, err
}

func (si *Identity) DeleteIdentity() error {
	err := si.KubeClient.PeerdiscoveryV1alpha1.
		Delete().
		Namespace(si.namespace).
		Name(si.clusterIdentityName).
		Resource(IdentitiesResource).
		Do(context.TODO()).Error()

	if errors.IsNotFound(err) {
		err = fmt.Errorf("identity not found")
		return err
	} else if err != nil {
		panic(err.Error())
	}

	return nil
}

func (si *Identity) UpdateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error) {
	result := v1alpha1.Identity{}
	identity.Name = si.clusterIdentityName
	err := si.KubeClient.PeerdiscoveryV1alpha1.
		Put().
		Namespace(si.namespace).
		Name(si.clusterIdentityName).
		Resource(IdentitiesResource).
		Body(identity).
		Do(context.TODO()).
		Into(&result)

	if errors.IsNotFound(err) {
		err = fmt.Errorf("identity not found")
		return nil, err
	} else if err != nil {
		panic(err.Error())
	}

	return &result, err
}

func (si *Identity) CheckIdentitySwarmPool(network string) bool {
	identity, err := si.GetIdentity()
	if err != nil {
		return false
	}

	identityNetworks := identity.Spec.PublicationTargets
	for _, identityNetwork := range identityNetworks {
		if identityNetwork.SwarmPoolName == network {
			return true
		}
	}

	return false
}

func (si *Identity) AddIdentitySwarmPool(network string) (*v1alpha1.Identity, error) {
	identity, err := si.GetIdentity()
	if err != nil {
		return nil, err
	}

	identityNetworks := identity.Spec.PublicationTargets
	for _, identityNetwork := range identityNetworks {
		if identityNetwork.SwarmPoolName == network {
			return identity, nil
		}
	}

	swarm := v1alpha1.TargetedSwarm{}
	swarm.Namespace = network
	swarm.SwarmPoolName = network
	swarm.Swarm = network
	identity.Spec.PublicationTargets = append(identityNetworks, swarm)

	identity, err = si.UpdateIdentity(identity)

	if errors.IsNotFound(err) {
		err = fmt.Errorf("identity not found")
		return nil, err
	} else if err != nil {
		panic(err.Error())
	}

	return identity, nil
}

func (si *Identity) DeleteIdentitySwarmPool(network string) (*v1alpha1.Identity, error) {
	identity, err := si.GetIdentity()
	if err != nil {
		return nil, err
	}

	toUpdate := false
	identityNetworks := identity.Spec.PublicationTargets
	for index, identityNetwork := range identityNetworks {
		if identityNetwork.SwarmPoolName == network {
			identity.Spec.PublicationTargets = append(identityNetworks[:index], identityNetworks[index+1:]...)
			toUpdate = true
		}
	}

	if toUpdate {
		identity, err = si.UpdateIdentity(identity)

		if errors.IsNotFound(err) {
			err = fmt.Errorf("identity not found")
			return nil, err
		} else if err != nil {
			panic(err.Error())
		}
	}

	return identity, nil
}
