package models

import (
	"context"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"k8s.io/apimachinery/pkg/api/errors"
)

const PeersResource = "Peers"

type IPeers interface {
	GetPeer(namespace string, name string) (*v1alpha1.Peer, error)
	ListPeers(namespace string) *[]v1alpha1.Peer
}

type Peers struct {
	KubeClient *common.ExtendedClientset
}

func GetPeers() IPeers {
	return &Peers{
		KubeClient: common.GetKubeClient(),
	}
}

func (sp *Peers) GetPeer(namespace string, name string) (*v1alpha1.Peer, error) {
	peer := v1alpha1.Peer{}
	err := sp.KubeClient.PeerdiscoveryV1alpha1.Get().Namespace(namespace).Resource(PeersResource).Name(name).Do(context.TODO()).Into(&peer)
	if errors.IsNotFound(err) {
		return &peer, err
	} else if err != nil {
		panic(err.Error())
	}
	return &peer, nil
}

func (sp *Peers) ListPeers(namespace string) *[]v1alpha1.Peer {
	peers := v1alpha1.PeerList{}
	err := sp.KubeClient.PeerdiscoveryV1alpha1.Get().Namespace(namespace).Resource(PeersResource).Do(context.TODO()).Into(&peers)
	if err != nil {
		panic(err.Error())
	}
	return &peers.Items
}
