package common

import (
	"fmt"
	"regexp"

	"github.com/gin-gonic/gin/binding"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

const DNSRegexStringRFC1123 = "^[a-z0-9]([-a-z0-9]*[a-z0-9])?$"

var DNSRegexRFC1123 = regexp.MustCompile(DNSRegexStringRFC1123)

func init() {
	v := binding.Validator.Engine().(*validator.Validate)
	trans := GetTranslator()
	_ = en_translations.RegisterDefaultTranslations(v, *trans)

	RegisterDNSRegexRFC1123Validator(v, trans)
	RegisterIsDefaultIfNotPrivateValidator(v, trans)
	RegisterRequiredWithTranslator(v, trans)
}

func RegisterDNSRegexRFC1123Validator(v *validator.Validate, trans *ut.Translator) {
	dnsRegexFn := func(fl validator.FieldLevel) bool {
		return DNSRegexRFC1123.MatchString(fl.Field().String())
	}
	v.RegisterValidation("dnsrfc1123", dnsRegexFn)

	registerFn := func(ut ut.Translator) error {
		return ut.Add("dnsrfc1123", fmt.Sprintf("{0} must be DNS RFC1123 regex compliant : %s", DNSRegexStringRFC1123), false)

	}
	transFn := func(ut ut.Translator, fe validator.FieldError) string {
		t, err := ut.T(fe.Tag(), fe.Field())
		if err != nil {
			return fe.(error).Error()
		}
		return t
	}
	v.RegisterTranslation("dnsrfc1123", *trans, registerFn, transFn)
}

func RegisterIsDefaultIfNotPrivateValidator(v *validator.Validate, trans *ut.Translator) {
	isDefaultIfNotPrivate := func(fl validator.FieldLevel) bool {
		field, _, _, found := fl.GetStructFieldOKAdvanced2(fl.Parent(), "IsPrivate")
		isPrivate := field.Bool()
		return found && ((!isPrivate && fl.Field().IsZero()) || isPrivate)
	}
	v.RegisterValidation("isdefault_ifnotprivate", isDefaultIfNotPrivate)

	registerFn := func(ut ut.Translator) error {
		return ut.Add("isdefault_ifnotprivate", "{0} must not be valued if network is not private", false)
	}
	transFn := func(ut ut.Translator, fe validator.FieldError) string {
		t, err := ut.T(fe.Tag(), fe.Field())
		if err != nil {
			return fe.(error).Error()
		}
		return t
	}
	v.RegisterTranslation("isdefault_ifnotprivate", *trans, registerFn, transFn)
}

func RegisterRequiredWithTranslator(v *validator.Validate, trans *ut.Translator) {
	registerFn := func(ut ut.Translator) error {
		return ut.Add("required_with", "{0} is required if {1} is valued", false)
	}
	transFn := func(ut ut.Translator, fe validator.FieldError) string {
		t, err := ut.T(fe.Tag(), fe.Field(), fe.Param())
		if err != nil {
			return fe.(error).Error()
		}
		return t
	}
	v.RegisterTranslation("required_with", *trans, registerFn, transFn)
}
