package common

import (
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"

	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type ExtendedClientset struct {
	kubernetes.Clientset

	PeerdiscoveryV1alpha1 *rest.RESTClient
}

var KubeClient *ExtendedClientset

func LoadKubeConfig() {
	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	v1alpha1.AddToScheme(scheme.Scheme)

	crdConfig := *config
	crdConfig.ContentConfig.GroupVersion = &v1alpha1.GroupVersion
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()
	restClient, err := rest.UnversionedRESTClientFor(&crdConfig)

	if err != nil {
		panic(err.Error())
	}

	clientset, err := kubernetes.NewForConfig(config)

	if err != nil {
		panic(err.Error())
	}

	KubeClient = &ExtendedClientset{
		Clientset:             *clientset,
		PeerdiscoveryV1alpha1: restClient,
	}
}

// Using this function to get a connection, you can create your connection pool here.
func GetKubeClient() *ExtendedClientset {
	return KubeClient
}
