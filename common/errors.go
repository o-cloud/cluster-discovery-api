package common

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

const (
	SwarmPoolBadRequestErrorCode   = "400001"
	IdentityBadRequestErrorCode    = "400002"
	SwarmPoolNotFoundErrorCode     = "404001"
	IdentityNotFoundErrorCode      = "404002"
	PeerNotFoundErrorCode          = "404003"
	SwarmPoolBadBodyErrorCode      = "422001"
	SwarmPoolAlreadyExistErrorCode = "422002"
	IdentityBadBodyErrorCode       = "422003"
	IdentityAlreadyExistErrorCode  = "422004"
)

type CommonError struct {
	Errors map[string]interface{} `json:"errors"`
}

func NewError(key string, err error) CommonError {
	res := CommonError{}
	res.Errors = make(map[string]interface{})
	res.Errors[key] = err.Error()
	return res
}

// To handle the error returned by c.Bind in gin framework
// https://github.com/go-playground/validator/blob/v9/_examples/translations/main.go
func NewValidatorError(key string, errs validator.ValidationErrors) CommonError {
	res := CommonError{}
	res.Errors = make(map[string]interface{})
	errsTab := make(map[string]interface{})
	for _, v := range errs {
		// can translate each error one at a time
		if v.Param() != "" {
			errsTab[v.Field()] = fmt.Sprintf("%v(%v): %v", v.Tag(), v.Param(), v.Translate(*trans))
		} else {
			errsTab[v.Field()] = fmt.Sprintf("%v: %v", v.Tag(), v.Translate(*trans))
		}

	}
	res.Errors[key] = errsTab
	return res
}
