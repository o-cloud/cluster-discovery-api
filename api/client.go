package api

import (
	"github.com/ShaileshSurya/hammer"
)

type ClusterDiscoveryApiClient struct {
	baseUrl    string
	restClient *hammer.Hammer
}

func NewClusterDiscoveryApiClient(baseUrl string) *ClusterDiscoveryApiClient {
	return &ClusterDiscoveryApiClient{
		baseUrl:    baseUrl,
		restClient: hammer.New(),
	}
}

func (pc *ClusterDiscoveryApiClient) ListPeers() (PeersResponse, error) {
	var result PeersResponse
	req, _ := hammer.RequestBuilder().
		Get().
		WithURL(pc.baseUrl + "/peers").
		Build()
	err := pc.restClient.ExecuteInto(req, &result)
	return result, err
}

func (pc *ClusterDiscoveryApiClient) ListIdentities() (IdentityResponse, error) {
	var result IdentityResponse
	req, err := hammer.RequestBuilder().
		Get().
		WithURL(pc.baseUrl + "/identity").
		Build()
	if err != nil {
		return result, err
	}
	err = pc.restClient.ExecuteInto(req, &result)
	return result, err
}
