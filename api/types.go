package api

//identity service types

type IdentitiesAPI interface {
	ListIdentities() (IdentityResponse, error)
}

type IdentityResponse struct {
	Identity Identity `json:"identity"`
}

type Identity struct {
	Name        string   `json:"name" binding:"required"`
	URL         string   `json:"url" binding:"required"`
	Description string   `json:"description"`
	Categories  []string `json:"categories"`
	Networks    []string `json:"networks" binding:"required"`
}

type PeersAPI interface {
	ListPeers() (PeersResponse, error)
}

type PeersResponse struct {
	Peers      []Peer `json:"peers"`
	PeersCount int    `json:"peers_count"`
}
type Peer struct {
	Network     string   `json:"network" binding:"required"`
	Id          string   `json:"id" binding:"required"`
	Name        string   `json:"name" binding:"required"`
	URL         string   `json:"url"`
	Description string   `json:"description"`
	Categories  []string `json:"categories"`
	Status      string   `json:"status"`
}

type NetworkState string

const (
	// NotReady is default state
	NetworkNotReady NetworkState = "NotReady"
	// Pending means the ipfs container is in Waiting state
	NetworkPending NetworkState = "Pending"
	// Ready means the ipfs container is in Running state
	NetworkReady NetworkState = "Ready"
	// InDeletion means the namespace is in Terminating state
	NetworkDeleting NetworkState = "Deleting"
)
