/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// IdentityStateValues represents the state of a the publicated identity (synchronized, i.e. published on the IPFS network, or not)
type IdentityStateValues string

// Category is used to specify whatever metadata one need to attach to their identity
type Category struct {
	Name string `json:"name"`
}

// Categories contains ... a list of category
type Categories []Category

// Identity states.
const (
	// IPFS data in sync with kubernetes object
	Synchronized IdentityStateValues = "Synchronized"
	// IPFS data out of sync with kubernetes object
	OutOfSync IdentityStateValues = "Out of sync"
)

// TargetedSwarm is the identity publication target (an IPFS node behind the scene)
type TargetedSwarm struct {
	Namespace     string `json:"namespace"`
	SwarmPoolName string `json:"swarmPool"`
	Swarm         string `json:"swarm"`
}

// TargetedSwarms is ... a list of TargetedSwarm
type TargetedSwarms []TargetedSwarm

// IdentitySpec defines the desired state of Identity
type IdentitySpec struct {
	PublicationTargets TargetedSwarms `json:"swarms"`
	URL                string         `json:"URL"`
	Description        string         `json:"description"`
	Categories         Categories     `json:"categories"`
	Name               string         `json:"name"`
}

type PeersSyncAndIdentitySync struct {
	IdentitySync IdentityStateValues `json:"identity status"`
	PeersSync    string              `json:"synchronized peers"`
}

// IdentityStatus defines the observed state of Identity
type IdentitiesStatus struct {
	IdentitiesStatus map[string]PeersSyncAndIdentitySync `json:"status"`
	LastUpdate       string                              `json:"lastUpdate"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope=Namespaced
// +kubebuilder:printcolumn:name="Name",type=string,JSONPath=`.spec.name`
// +kubebuilder:printcolumn:name="URL",type=string,JSONPath=`.spec.URL`
// Identity is the Schema for the identities API
type Identity struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   IdentitySpec     `json:"spec,omitempty"`
	Status IdentitiesStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// IdentityList contains a list of Identity
type IdentityList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Identity `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Identity{}, &IdentityList{})
}
