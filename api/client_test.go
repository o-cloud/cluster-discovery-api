package api

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"testing"

	"github.com/ShaileshSurya/hammer"
)

func TestInterface(t *testing.T) {
	cdac := NewClusterDiscoveryApiClient("http://irtsb.ml/api/discovery")
	_ = PeersAPI(cdac)
	_ = IdentitiesAPI(cdac)
}

type MockClient struct {
	err      error
	response *http.Response
}

func (m MockClient) Do(req *http.Request) (*http.Response, error) {
	if m.err != nil {
		return nil, m.err
	}
	return m.response, nil
}

func TestListPeers(t *testing.T) {
	b, _ := json.Marshal(PeersResponse{[]Peer{{"net", "id", "name", "url", "desc", []string{"cat"}, "stat"}}, 1})
	hammer := GetHammerWithBodyReturn(b)
	cdac := &ClusterDiscoveryApiClient{
		baseUrl:    "http://localhost:1234/",
		restClient: hammer,
	}
	res, err := cdac.ListPeers()
	if err != nil {
		t.Error(err)
	}
	t.Log(res)
}

func TestListIdentities(t *testing.T) {
	b, _ := json.Marshal(IdentityResponse{Identity{"nam", "url", "desc", []string{"cat"}, []string{}}})
	hammer := GetHammerWithBodyReturn(b)
	cdac := &ClusterDiscoveryApiClient{
		baseUrl:    "http://localhost:1234/",
		restClient: hammer,
	}
	res, err := cdac.ListIdentities()
	if err != nil {
		t.Error(err)
	}
	t.Log(res)
}

func GetHammerWithBodyReturn(body []byte) *hammer.Hammer {
	return &hammer.Hammer{
		HTTPClient: MockClient{
			//err: errors.New("Error"),
			response: &http.Response{
				Status:     "200 OK",
				StatusCode: 200,
				Body: struct {
					io.Reader
					io.Closer
				}{
					io.MultiReader(bytes.NewReader(body), http.NoBody),
					http.NoBody,
				},
			},
		},
	}
}
