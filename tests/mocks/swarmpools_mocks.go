package mocks

import (
	"fmt"

	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
)

type MockSwarmPools struct {
	Called    string
	RetArray  []v1alpha1.SwarmPool
	RetString string
	Err       error
}

func (msp *MockSwarmPools) ListSwarmPools() *[]v1alpha1.SwarmPool {
	msp.Called = msp.Called + "ListSwarmPools()"
	return &msp.RetArray
}

func (msp *MockSwarmPools) GetSwarmPool(namespace string) (*v1alpha1.SwarmPool, error) {
	msp.Called = msp.Called + fmt.Sprintf("GetSwarmPool(%s)", namespace)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockSwarmPools) CreateSwarmPool(swarmpool *v1alpha1.SwarmPool, privateKey string) (*v1alpha1.SwarmPool, error) {
	msp.Called = msp.Called + fmt.Sprintf("CreateSwarmPool(%s,%s)", swarmpool.Namespace, privateKey)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockSwarmPools) DeleteSwarmPool(namespace string) error {
	msp.Called = msp.Called + fmt.Sprintf("DeleteSwarmPool(%s)", namespace)
	return msp.Err
}

func (msp *MockSwarmPools) UpdateSwarmPool(namespace string, swarmpool *v1alpha1.SwarmPool) (*v1alpha1.SwarmPool, error) {
	msp.Called = msp.Called + fmt.Sprintf("UpdateSwarmPool(%s,%s)", namespace, swarmpool.Namespace)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockSwarmPools) GetSwarmPoolPrivateKey(namespace string, secretName string) string {
	msp.Called = msp.Called + fmt.Sprintf("GetSwarmPoolPrivateKey(%s,%s)", namespace, secretName)
	return "random-privatekey"
}

func (msp *MockSwarmPools) CreateSwarmPoolSecret(namespace string, secretName string, privateKey string) {
	msp.Called = msp.Called + fmt.Sprintf("CreateSwarmPoolSecret(%s,%s,%s)", namespace, secretName, privateKey)
}

func (msp *MockSwarmPools) GetDefaultNetworkName() string {
	msp.Called = msp.Called + "GetDefaultNetworkName()"
	return msp.RetString
}

func (msp *MockSwarmPools) GetNetworkState(namespace string) (api.NetworkState, error) {
	msp.Called = msp.Called + fmt.Sprintf("GetNetworkState(%s)", namespace)
	return api.NetworkReady, msp.Err
}
