package mocks

import (
	"fmt"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
)

type MockIdentity struct {
	Called   string
	RetArray []v1alpha1.Identity
	Err      error
}

func (msp *MockIdentity) GetIdentity() (*v1alpha1.Identity, error) {
	msp.Called = msp.Called + "GetIdentity()"
	return &msp.RetArray[0], msp.Err
}

func (msp *MockIdentity) CreateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error) {
	msp.Called = msp.Called + fmt.Sprintf("CreateIdentity(%s)", identity.Spec.Name)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockIdentity) DeleteIdentity() error {
	msp.Called = msp.Called + "DeleteIdentity()"
	return msp.Err
}

func (msp *MockIdentity) UpdateIdentity(identity *v1alpha1.Identity) (*v1alpha1.Identity, error) {
	msp.Called = msp.Called + fmt.Sprintf("UpdateIdentity(%s)", identity.Name)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockIdentity) AddIdentitySwarmPool(network string) (*v1alpha1.Identity, error) {
	msp.Called = msp.Called + fmt.Sprintf("AddIdentitySwarmPool(%s)", network)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockIdentity) DeleteIdentitySwarmPool(network string) (*v1alpha1.Identity, error) {
	msp.Called = msp.Called + fmt.Sprintf("DeleteIdentitySwarmPool(%s)", network)
	return &msp.RetArray[0], msp.Err
}

func (msp *MockIdentity) CheckIdentitySwarmPool(network string) bool {
	msp.Called = msp.Called + fmt.Sprintf("CheckIdentitySwarmPool(%s)", network)
	return true
}
