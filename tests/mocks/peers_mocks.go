package mocks

import (
	"fmt"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
)

type MockPeers struct {
	Called   string
	RetArray []v1alpha1.Peer
	Err      error
}

func (mpeers *MockPeers) ListPeers(namespace string) *[]v1alpha1.Peer {
	mpeers.Called = mpeers.Called + fmt.Sprintf("ListPeers(%s)", namespace)
	return &mpeers.RetArray
}

func (mpeers *MockPeers) GetPeer(namespace string, name string) (*v1alpha1.Peer, error) {
	mpeers.Called = mpeers.Called + fmt.Sprintf("GetPeer(%s,%s)", namespace, name)
	return &mpeers.RetArray[0], mpeers.Err
}
