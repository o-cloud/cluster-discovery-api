[![pipeline status](https://gitlab.com/o-cloud/cluster-discovery-api/badges/master/pipeline.svg)](https://gitlab.com/o-cloud/cluster-discovery-api/commits/master)
[![coverage report](https://gitlab.com/o-cloud/cluster-discovery-api/badges/master/coverage.svg)](https://gitlab.com/o-cloud/cluster-discovery-api/commits/master)

# Project Cluster discovery API

## Prerequisites

*helm
*skaffold
*kubectl

A kubernetes cluster in kubectl current context is needed to use this api with Skaffold.

## Usage

### Run

```bash
skaffold run
```

Option *default-repo* need to be added in skaffold command for specify container registry to used.

### Dev

```bash
skaffold dev
```

## Test

From the project root, run :

```go
go test ./... -cover
```

## Configuration

### Server

| Name          | Type   | Default |
|---------------|--------|---------|
| listenAddress | string | ":9090"
| apiPrefix     | string | "/api/discovery"

### Traefik

| Name                  | Type   | Default      | Description |
|-----------------------|--------|--------------|-------------|
| loadBalancerService   | string | "traefik"    | Kubernetes service name of Traefik
| loadBalancerNamespace | string | "traefik-v2" | Kubernetes service namespace of Traefik

### IPFS

| Name      | Type   | Default | Description |
|-----------|--------|---------|-------------|
| version   | string | 0.8.0   | IPFS version of deployed swarmpools
| swarmPort | int    | 4001    | IPFS port of deployed swarmpools
| apiPort   | int    | 5001    | IPFS api port of deployed swarmpools

## Routes

| Method | Route |
|--------|-------|
| GET    | [*prefix*/identity](#get-identity)
| POST   | [*prefix*/identity](#post-identity)
| PUT    | [*prefix*/identity](#put-identity)
| DELETE | [*prefix*/identity](#delete-identity)
| GET    | [*prefix*/peers](#get-peers)
| GET    | [*prefix*/networks](#get-networks)
| GET    | [*prefix*/networks/:network](#get-networksnetwork)
| GET    | [*prefix*/networks/:network/connection-info](#get-networksnetworkconnection-info)
| GET    | [*prefix*/networks/:network/status](#get-networksnetworkstatus)
| POST   | [*prefix*/networks](#post-networks)
| DELETE | [*prefix*/networks/:network](#delete-networksnetwork)
| GET    | [*prefix*/networks/:network/peers](#get-networksnetworkpeers)
| GET    | [*prefix*/networks/:network/peers/:peer](#get-networksnetworkpeerspeer)

### GET /identity

Get the identity of the cluster.  

Response :

| Field                | Type            | Description |
|----------------------|-----------------|-------------|
| identity             | object          | /
| \\____ name          | string          | name of the identity
| \\____ url           | string          | api url of the identity
| \\____ description   | string          | description of the identity
| \\____ categories    | array of string | categories of services provided by the identity
| \\____ networks      | array of string | name of the networks that use this identity

Response example:

```yaml
{
    "identity": {
        "name": "identity-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ],
        "networks": {
            "name-of-network-1",
            "name-of-network-2"
        }
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404002        | Identity not found

### POST /identity

Create an identity on the cluster. The identity is attached on all the existant networks.  

Body :

| Field                | Required | Type            | Description |
|----------------------|----------|-----------------|-------------|
| identity             | Yes      | object          | /
| \\____ name          | Yes      | string          | name of the identity
| \\____ url           | Yes      | string          | api url of the identity
| \\____ description   | No       | string          | description of the identity
| \\____ categories    | No       | array of string | categories of services provided by the identity

Body example:

```yaml
{
    "identity": {
        "name": "identity-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ]
    }
}
```

Response :

| Field                | Type            | Description |
|----------------------|-----------------|-------------|
| identity             | object          | /
| \\____ name          | string          | name of the identity
| \\____ url           | string          | api url of the identity
| \\____ description   | string          | description of the identity
| \\____ categories    | array of string | categories of services provided by the identity
| \\____ networks      | array of string | name of the networks that use this identity

Response example:

```yaml
{
    "identity": {
        "name": "identity-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ],
        "networks": {
            "name-of-network-1",
            "name-of-network-2"
        }
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 201       | /             | Created
| 400       | 400002        | Unable to parse the JSON body
| 422       | 422003        | Unable to create the identity ; required field(s) may be absent
| 422       | 422004        | Identity already exists

### PUT /identity

Update the cluster identity.  

Body :

| Field                | Required | Type            | Description |
|----------------------|----------|-----------------|-------------|
| identity             | Yes      | object          | /
| \\____ name          | Yes      | string          | name of the identity
| \\____ url           | Yes      | string          | api url of the identity
| \\____ description   | No       | string          | description of the identity
| \\____ categories    | No       | array of string | categories of services provided by the identity

Body example:

```yaml
{
    "identity": {
        "name": "identity-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ]
    }
}
```

Response :

| Field                | Type            | Description |
|----------------------|-----------------|-------------|
| identity             | object          | /
| \\____ name          | string          | name of the identity
| \\____ url           | string          | api url of the identity
| \\____ description   | string          | description of the identity
| \\____ categories    | array of string | categories of services provided by the identity
| \\____ networks      | array of string | name of the networks that use this identity

Response example:

```yaml
{
    "identity": {
        "name": "identity-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ],
        "networks": {
            "name-of-network-1",
            "name-of-network-2"
        }
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 201       | /             | Created
| 400       | 400002        | Unable to parse the JSON body
| 404       | 422004        | Identity not found
| 422       | 422003        | Unable to create the identity ; required field(s) may be absent

### DELETE /identity

Delete the cluster identity. The cluster will not ever be visible by other cluster.

Response : no response

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 204       | /             | Deleted
| 404       | 404002        | Identity not found

### GET /peers

List all the connected peers on the cluster.  

Response :

| Field              | Type              | Description |
|--------------------|-------------------|-------------|
| peers              | array of peer     | array of peers
| \\____ network     | string            | network in which the peer is connected
| \\____ id          | string            | identifier of the peer
| \\____ name        | string            | name of the peer
| \\____ url         | string            | api url of the peer
| \\____ description | string            | description of the peer
| \\____ categories  | array of string   | categories of services provided by the peer
| \\____ status      | string            | status of the peer (Online, Offline, Unknown)
| peers_count        | number            | number of peers

Response example:

```yaml
{
    "peers": [
        {
            "network": "peer-network",
            "id": "peer-identifier",
            "name": "peer-name",
            "url": "https://google.com",
            "description": "Peer description in the network",
            "categories": [
                "data category 1",
                "compute category 2",
                "data category 3"
            ],
            "status": "Online",
        }
    ],
    "peers_count": 1
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success

### GET /networks

List all the created network on the cluster.  

Response :

| Field                | Type             | Description |
|----------------------|------------------|-------------|
| networks             | array of network | array of networks
| \\____ name          | string           | name of the network
| \\____ is_private    | bool             | true if network is a private network
| \\____ joining_nodes | array of string  | array of peers that is use on bootstrap to join a private network
| \\____ entrypoint    | string           | cluster entrypoint name for the network
| networks_count       | number           | number of networks

Response example:

```yaml
{
    "networks": [
        {
            "name": "network-name",
            "is_private": true,
            "joining_nodes": ["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],
            "entrypoint": "ipfs"
        }
    ],
    "networks_count": 1
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success

### GET /networks/:network

Get the created network with requested name.

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the requested network

Response :

| Field                | Type            | Description |
|----------------------|-----------------|-------------|
| network              | object          | /
| \\____ name          | string          | name of the network
| \\____ is_private    | bool            | true if network is a private network
| \\____ joining_nodes | array of string | array of peers that is use on bootstrap to join a private network
| \\____ entrypoint    | string          | cluster entrypoint name for the network

Response example:

```yaml
{
    "network": {
        "name": "network-name",
        "is_private": true,
        "joining_nodes": ["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],
        "entrypoint": "ipfs"
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404001        | Requested network not found

### GET /networks/:network/connection-info

Get the connection info of the network with requested name.

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the requested network

Response :

| Field               | Type            | Description |
|---------------------|-----------------|-------------|
| connection_info     | object          | /
| \\____ network_name | string          | name of the network
| \\____ private_key  | string          | private key of the network
| \\____ peer_id      | array of string | peer id to add in joining node to use this cluser on bootstrap

Response example:

```yaml
{
    "connection_info": {
        "network_name": "network-name",
        "private_key": "L2tleS9zd2FybS9wc2svMS4wLjAvCi9iYXNlMTYvCjBhOGVhY2E2ODcxOWNkYjJlZjcxMDE4MDY5YmQ3MDk4ZmQ0MmUxNjZmODkxMTYxZjM2NjI4Yjc3YjUyOWVkNTkK",
        "peer_id": ["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404001        | Requested network not found

### GET /networks/:network/status

Get the status of the network with requested name.

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the requested network

Response :

| Field                        | Type   | Description |
|------------------------------|--------|-------------|
| status                       | object | /
| \\____ state                 | string | One of Ready, Pending, NotReady, Deleting
| \\____ is_identity_published | bool   | true if network is attached to identity

Response example:

```yaml
{
    "status": {
        "state": "Ready",
        "is_identity_published": true,
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404001        | Requested network not found

### POST /networks

Create a network. A network can be default or private network. Default network use default private key and default joining nodes. Private network need a custom private key or generate it if not submited. Only one default network can be created on the cluster.

Body :

| Field                | Type            | Default | Required | Description |
|----------------------|-----------------|---------|----------|-------------|
| network              | object          |         | Yes      | /
| \\____ name          | string          |         | Yes      | name of the network configuration
| \\____ is_private    | bool            | false   | No       | true if network is a private network
| \\____ private_key   | string          |         | No       | private key of the network, required if joining_nodes not empty, generated if empty and is_private true
| \\____ joining_nodes | array of string |         | No       | array of peers that is use on bootstrap to join a private network
| \\____ entrypoint    | string          |         | Yes      | cluster entrypoint name for the network

Response : Same as body

Response and body example:

```yaml
{
    "network": {
        "name": "network-name",
        "is_private": true,
        "joining_nodes": ["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],
        "entrypoint": "ipfs"
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 201       | /             | Created
| 400       | 400001        | Unable to parse the JSON body
| 422       | 422001        | Unable to create the network ; required field(s) may be absent
| 422       | 422002        | Network already exists

### DELETE /networks/:network

Delete the network with requested name.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the requested network to delete

Response : no response

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 204       | /             | Deleted
| 404       | 404001        | Requested network not found

### GET /networks/:network/peers

List all the connected peers in the network with name in parameter.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the concerned network

Response :

| Field              | Type              | Description |
|--------------------|-------------------|-------------|
| peers              | array of peer     | array of peers
| \\____ network     | string            | network in which the peer is connected
| \\____ id          | string            | identifier of the peer
| \\____ name        | string            | name of the peer
| \\____ url         | string            | api url of the peer
| \\____ description | string            | description of the peer
| \\____ categories  | array of string   | categories of services provided by the peer
| \\____ status      | string            | status of the peer (Online, Offline, Unknown)
| peers_count        | number            | number of peers

Response example:

```yaml
{
    "peers": [
        {
            "network": "peer-network",
            "id": "peer-identifier",
            "name": "peer-name",
            "url": "https://google.com",
            "description": "Peer description in the network",
            "categories": [
                "data category 1",
                "compute category 2",
                "data category 3"
            ],
            "status": "Online",
        }
    ],
    "peers_count": 1
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success

### GET /networks/:network/peers/:peer

Get the peer in the network with name in parameter.  

Parameter :

| Name    | Type   | Required | Description |
|---------|--------|----------|-------------|
| network | string | Yes      | name of the concerned network
| peer    | string | Yes      | identifier of the peer

Response :

| Field              | Type            | Description |
|--------------------|-----------------|-------------|
| peer               | object          | /
| \\____ network     | string          | network in which the peer is connected
| \\____ id          | string          | identifier of the peer
| \\____ name        | string          | name of the peer
| \\____ url         | string          | api url of the peer
| \\____ description | string          | description of the peer
| \\____ categories  | array of string | categories of services provided by the peer
| \\____ status      | string          | status of the peer (Online, Offline, Unknown)

Response example:

```yaml
{
    "peer": {
        "network": "peer-network",
        "id": "peer-identifier",
        "name": "peer-name",
        "url": "https://google.com",
        "description": "Peer description in the network",
        "categories": [
            "data category 1",
            "compute category 2",
            "data category 3"
        ],
        "status": "Online",
    }
}
```

Return codes :

| HTTP code | Internal code | Description |
|-----------|---------------|-------------|
| 200       | /             | Success
| 404       | 404003        | Peer not found in concerned network
