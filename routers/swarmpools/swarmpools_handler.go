package routers

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type SwarmPoolsHandler struct {
	S models.ISwarmPools
	I models.IIdentity
}

func NewSwarmPoolsHandler() *SwarmPoolsHandler {
	return &SwarmPoolsHandler{
		S: models.GetSwarmPools(),
		I: models.GetIdentity(),
	}
}

func (S *SwarmPoolsHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/:network", S.handleGet)
	router.GET("/:network/connection-info", S.handleGetConnectionInfo)
	router.GET("/:network/status", S.handleGetStatus)
	router.GET("", S.handleList)
	router.POST("", S.handleCreate)
	router.PUT("/:network", S.handleUpdate)
	router.DELETE("/:network", S.handleDelete)
}

// Return the requested SwarmPool
func (S *SwarmPoolsHandler) handleGet(c *gin.Context) {
	network := c.Param("network")
	swarmpool, err := S.S.GetSwarmPool(network)
	if err != nil {
		body := common.NewError(common.SwarmPoolNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}
	serializer := SwarmPoolSerializer{c, *swarmpool}
	c.JSON(http.StatusOK, gin.H{"network": serializer.Response()})
}

// Return the requested SwarmPool status
func (S *SwarmPoolsHandler) handleGetStatus(c *gin.Context) {
	network := c.Param("network")
	networkState, err := S.S.GetNetworkState(network)
	if err != nil {
		body := common.NewError(common.SwarmPoolNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	isIdentityPublished := S.I.CheckIdentitySwarmPool(network)

	serializer := SwarmPoolStatusSerializer{c, networkState, isIdentityPublished}
	c.JSON(http.StatusOK, gin.H{"status": serializer.Response()})
}

// Return the requested SwarmPool Connection Info
func (S *SwarmPoolsHandler) handleGetConnectionInfo(c *gin.Context) {
	network := c.Param("network")
	swarmpool, err := S.S.GetSwarmPool(network)
	if err != nil {
		body := common.NewError(common.SwarmPoolNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	// Get private key for swarmpool
	swarmKeySecretRef := swarmpool.Spec.Swarms[0].SwarmDefinition.SwarmKeySecretRef
	privateKey := S.S.GetSwarmPoolPrivateKey(network, swarmKeySecretRef)
	serializer := SwarmPoolConnectionInfoSerializer{c, *swarmpool, privateKey}
	c.JSON(http.StatusOK, gin.H{"connection_info": serializer.Response()})
}

// Return all the SwarmPools
func (S *SwarmPoolsHandler) handleList(c *gin.Context) {
	swarmpools := S.S.ListSwarmPools()
	serializer := SwarmPoolsSerializer{c, *swarmpools}
	c.JSON(http.StatusOK, gin.H{"networks": serializer.Response(), "networks_count": len(*swarmpools)})
}

// Create the submited SwarmPool
func (S *SwarmPoolsHandler) handleCreate(c *gin.Context) {

	swarmPoolModelValidator := NewSwarmPoolModelValidator()
	err := swarmPoolModelValidator.Bind(c)
	if err != nil {
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			body := common.NewError(common.SwarmPoolBadRequestErrorCode, errors.New("bad request : unable to parse json body"))
			c.JSON(http.StatusBadRequest, body)
			return
		}
		body := common.NewValidatorError(common.SwarmPoolBadBodyErrorCode, errs)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	swarmKeySecretRef := swarmPoolModelValidator.swarmPoolModel.Spec.Swarms[0].SwarmDefinition.SwarmKeySecretRef
	boostrapNodes := swarmPoolModelValidator.swarmPoolModel.Spec.Swarms[0].SwarmDefinition.BootstrapNodeAddresses
	// If default network we check that one not already exist
	if swarmKeySecretRef == "" && len(boostrapNodes) == 0 {
		defaultNetworkName := S.S.GetDefaultNetworkName()

		if defaultNetworkName != "" {
			err := fmt.Errorf("unable to create default network %s : there is already one named %s", swarmPoolModelValidator.SwarmPool.Name, defaultNetworkName)
			body := common.NewError(common.SwarmPoolBadBodyErrorCode, err)
			c.JSON(http.StatusUnprocessableEntity, body)
			return
		}
	}

	swarmpool, err := S.S.CreateSwarmPool(&swarmPoolModelValidator.swarmPoolModel, swarmPoolModelValidator.SwarmPool.PrivateKey)
	if err != nil {
		body := common.NewError(common.SwarmPoolAlreadyExistErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	S.I.AddIdentitySwarmPool(swarmpool.Namespace)

	serializer := SwarmPoolSerializer{c, *swarmpool}
	c.JSON(http.StatusCreated, gin.H{"network": serializer.Response()})
}

// Update the submited SwarmPool
func (S *SwarmPoolsHandler) handleUpdate(c *gin.Context) {
	c.String(http.StatusNotImplemented, "Update swarmPool not implemented")
}

// Delete the requested SwarmPool
func (S *SwarmPoolsHandler) handleDelete(c *gin.Context) {
	network := c.Param("network")

	S.I.DeleteIdentitySwarmPool(network)

	err := S.S.DeleteSwarmPool(network)
	if err != nil {
		body := common.NewError(common.SwarmPoolNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}
	c.Status(http.StatusNoContent)
}
