package routers_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/common"
	routers "gitlab.com/o-cloud/cluster-discovery-api/routers/swarmpools"
	"gitlab.com/o-cloud/cluster-discovery-api/tests/mocks"

	"github.com/gin-gonic/gin"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	msp    *mocks.MockSwarmPools
	msi    *mocks.MockIdentity
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.Default()
	msp := &mocks.MockSwarmPools{}
	msi := &mocks.MockIdentity{}
	(&routers.SwarmPoolsHandler{S: msp, I: msi}).SetupRoutes(r.Group("/networks"))
	return testSetup{
		t:      t,
		engine: r,
		msp:    msp,
		msi:    msi,
		req:    req,
		rr:     rr,
	}
}

func TestHandleListSwarmPools(t *testing.T) {
	test := getSetup(t, "GET", "/networks", nil)
	test.msp.RetArray = make([]v1alpha1.SwarmPool, 2)

	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: "ipfs"}},
	}

	test.msp.RetArray[0] = v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: "test-network", Namespace: "test-network"},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
	}

	swarms2 := make([]v1alpha1.Swarm, 1)
	swarms2[0] = v1alpha1.Swarm{
		SwarmDefinition: v1alpha1.SwarmDefinition{
			BootstrapNodeAddresses: []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"},
			SwarmKeySecretRef:      "network-key-ref",
		},
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: "ipfs2"}},
	}

	test.msp.RetArray[1] = v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: "test-network", Namespace: "test-network"},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms2},
	}

	test.run()
	test.expectMspMethodCall("ListSwarmPools()")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"networks":[{"name":"test-network","is_private":false,"entrypoint":"ipfs"},{"name":"test-network","is_private":true,"joining_nodes":["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],"entrypoint":"ipfs2"}],"networks_count":` + strconv.Itoa(len(test.msp.RetArray)) + `}`)
}

func TestHandleListSwarmpoolsEmpty(t *testing.T) {
	test := getSetup(t, "GET", "/networks", nil)
	test.run()
	test.expectMspMethodCall("ListSwarmPools()")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"networks":[],"networks_count":0}`)
}

func TestHandleGetSwarmPool(t *testing.T) {
	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		SwarmDefinition: v1alpha1.SwarmDefinition{
			BootstrapNodeAddresses: []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"},
			SwarmKeySecretRef:      "network-key-ref",
		},
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: "ipfs"}},
	}
	swarmpool := v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: "test-network", Namespace: "test-network"},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
	}

	test := getSetup(t, "GET", "/networks/"+swarmpool.Namespace, nil)
	test.msp.RetArray = []v1alpha1.SwarmPool{swarmpool}
	test.run()
	test.expectMspMethodCall(`GetSwarmPool(` + swarmpool.Namespace + `)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"network":{"name":"test-network","is_private":true,"joining_nodes":["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],"entrypoint":"ipfs"}}`)
}

func TestHandleGetSwarmPoolNotFound(t *testing.T) {
	test := getSetup(t, "GET", "/networks/randomkey", nil)
	test.msp.RetArray = make([]v1alpha1.SwarmPool, 1)
	test.msp.Err = errors.New("erreur")
	test.run()
	test.expectMspMethodCall(`GetSwarmPool(randomkey)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolNotFoundErrorCode + `":"` + test.msp.Err.Error() + `"}}`)
}

func TestHandleGetSwarmPoolConnectionInfo(t *testing.T) {
	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		SwarmDefinition: v1alpha1.SwarmDefinition{
			BootstrapNodeAddresses: []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"},
			SwarmKeySecretRef:      "network-key-ref",
		},
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: "ipfs"}},
	}
	swarmsStatus := map[string]v1alpha1.SwarmStatus{}
	swarmsStatus["test-network"] = v1alpha1.SwarmStatus{
		Addresses: []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"},
	}
	swarmpool := v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: "test-network", Namespace: "test-network"},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
		Status: v1alpha1.SwarmPoolStatus{
			SwarmsStatus: swarmsStatus,
		},
	}

	test := getSetup(t, "GET", "/networks/"+swarmpool.Namespace+"/connection-info", nil)
	test.msp.RetArray = []v1alpha1.SwarmPool{swarmpool}
	test.run()
	test.expectMspMethodCall(`GetSwarmPool(` + swarmpool.Namespace + `)GetSwarmPoolPrivateKey(` + swarmpool.Namespace + `,` + swarms[0].SwarmDefinition.SwarmKeySecretRef + `)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"connection_info":{"network_name":"test-network","private_key":"random-privatekey","peer_id":["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"]}}`)
}

func TestHandleGetSwarmPoolConnectionInfoNotFound(t *testing.T) {
	test := getSetup(t, "GET", "/networks/randomkey/connection-info", nil)
	test.msp.RetArray = make([]v1alpha1.SwarmPool, 1)
	test.msp.Err = errors.New("erreur")
	test.run()
	test.expectMspMethodCall(`GetSwarmPool(randomkey)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolNotFoundErrorCode + `":"` + test.msp.Err.Error() + `"}}`)
}

func TestHandleGetSwarmPoolStatus(t *testing.T) {
	test := getSetup(t, "GET", "/networks/random-network/status", nil)
	test.run()
	test.expectMspMethodCall(`GetNetworkState(random-network)`)
	test.expectMsiMethodCall(`CheckIdentitySwarmPool(random-network)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"status":{"state":"Ready","is_identity_published":true}}`)
}

func TestHandleGetSwarmPoolStatusNotFound(t *testing.T) {
	test := getSetup(t, "GET", "/networks/random-network/status", nil)
	test.msp.Err = errors.New("erreur")
	test.run()
	test.expectMspMethodCall(`GetNetworkState(random-network)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolNotFoundErrorCode + `":"` + test.msp.Err.Error() + `"}}`)
}

func TestHandleDeleteSwarmPool(t *testing.T) {
	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: "ipfs"}},
	}
	swarmpool := v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: "test-network", Namespace: "test-network"},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
	}

	test := getSetup(t, "DELETE", "/networks/"+swarmpool.Namespace, nil)
	test.msp.RetArray = []v1alpha1.SwarmPool{swarmpool}
	test.msi.RetArray = make([]v1alpha1.Identity, 1)

	test.run()
	test.expectMspMethodCall(`DeleteSwarmPool(` + swarmpool.Namespace + `)`)
	test.expectMsiMethodCall(`DeleteIdentitySwarmPool(` + swarmpool.Namespace + `)`)
	test.expectResponseStatus(http.StatusNoContent)
	test.expectBodyContent(``)
}

func TestHandleDeleteSwarmPoolNotFound(t *testing.T) {
	test := getSetup(t, "DELETE", "/networks/randomkey", nil)
	test.msp.RetArray = make([]v1alpha1.SwarmPool, 1)
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msp.Err = errors.New("erreur")
	test.run()
	test.expectMspMethodCall(`DeleteSwarmPool(randomkey)`)
	test.expectMsiMethodCall(`DeleteIdentitySwarmPool(randomkey)`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolNotFoundErrorCode + `":"` + test.msp.Err.Error() + `"}}`)
}

func TestHandleCreateSwarmPoolDefault(t *testing.T) {
	body := routers.SwarmPoolModelValidator{}
	body.SwarmPool.Name = "test-network"
	body.SwarmPool.EntryPoint = "ipfs"

	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: body.SwarmPool.EntryPoint}},
	}
	swarmpool := v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: body.SwarmPool.Name, Namespace: body.SwarmPool.Name},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
	}

	test := getSetup(t, "POST", "/networks", body)
	test.msp.RetArray = []v1alpha1.SwarmPool{swarmpool}
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msp.RetString = ""

	test.run()
	test.expectMspMethodCall(`GetDefaultNetworkName()CreateSwarmPool(` + swarmpool.Namespace + `,)`)
	test.expectMsiMethodCall(`AddIdentitySwarmPool(` + swarmpool.Namespace + `)`)
	test.expectResponseStatus(http.StatusCreated)
	test.expectBodyContent(`{"network":{"name":"test-network","is_private":false,"entrypoint":"ipfs"}}`)
}

func TestHandleCreateSwarmPoolPrivate(t *testing.T) {
	body := routers.SwarmPoolModelValidator{}
	body.SwarmPool.Name = "test-network"
	body.SwarmPool.EntryPoint = "ipfs"
	body.SwarmPool.IsPrivate = true
	body.SwarmPool.PrivateKey = "L2tleS9zd2FybS9wc2svMS4wLjAvCi9iYXNlMTYvCjBhOGVhY2E2ODcxOWNkYjJlZjcxMDE4MDY5YmQ3MDk4ZmQ0MmUxNjZmODkxMTYxZjM2NjI4Yjc3YjUyOWVkNTkK"
	body.SwarmPool.JoiningNodes = []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"}

	swarms := make([]v1alpha1.Swarm, 1)
	swarms[0] = v1alpha1.Swarm{
		SwarmDefinition: v1alpha1.SwarmDefinition{
			BootstrapNodeAddresses: []string{"/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"},
			SwarmKeySecretRef:      "network-key-ref",
		},
		Network: v1alpha1.SwarmNetwork{Traefik: v1alpha1.TraefikIntegrationDefinition{TraefikEntryPoint: body.SwarmPool.EntryPoint}},
	}
	swarmpool := v1alpha1.SwarmPool{
		ObjectMeta: metav1.ObjectMeta{Name: body.SwarmPool.Name, Namespace: body.SwarmPool.Name},
		Spec:       v1alpha1.SwarmPoolSpec{Swarms: swarms},
	}

	test := getSetup(t, "POST", "/networks", body)
	test.msp.RetArray = []v1alpha1.SwarmPool{swarmpool}
	test.msi.RetArray = make([]v1alpha1.Identity, 1)

	test.run()
	test.expectMspMethodCall(`CreateSwarmPool(` + swarmpool.Namespace + `,` + body.SwarmPool.PrivateKey + `)`)
	test.expectMsiMethodCall(`AddIdentitySwarmPool(` + swarmpool.Namespace + `)`)
	test.expectResponseStatus(http.StatusCreated)
	test.expectBodyContent(`{"network":{"name":"test-network","is_private":true,"joining_nodes":["/ip4/x.x.x.x/tcp/4001/p2p/QmRtpJacgqW3uD2VbeVnxAgXkiGtP8u3JUZ74sMjz3ebJm"],"entrypoint":"ipfs"}}`)
}

func TestHandleCreateSwarmPoolBadRequest(t *testing.T) {
	test := getSetup(t, "POST", "/networks", "plop")
	test.run()
	test.expectMspMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolBadRequestErrorCode + `":"bad request : unable to parse json body"}}`)
}

func TestHandleCreateSwarmPoolErrorValidation(t *testing.T) {
	test := getSetup(t, "POST", "/networks", v1alpha1.SwarmPool{})
	test.run()
	test.expectMspMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolBadBodyErrorCode + `":{"EntryPoint":"required: EntryPoint is a required field","Name":"required: Name is a required field"}}}`)
}

func TestHandleCreateSwarmPoolAlreadyExist(t *testing.T) {
	body := routers.SwarmPoolModelValidator{}
	body.SwarmPool.Name = "test-network"
	body.SwarmPool.EntryPoint = "ipfs"

	test := getSetup(t, "POST", "/networks", body)
	test.msp.RetArray = make([]v1alpha1.SwarmPool, 1)
	test.msp.RetString = ""
	test.msp.Err = errors.New("Erreur")
	test.run()
	test.expectMspMethodCall(`GetDefaultNetworkName()CreateSwarmPool(` + body.SwarmPool.Name + `,)`)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolAlreadyExistErrorCode + `":"` + test.msp.Err.Error() + `"}}`)
}

func TestHandleCreateSwarmPoolDefaultAlreadyExist(t *testing.T) {
	body := routers.SwarmPoolModelValidator{}
	body.SwarmPool.Name = "test-network"
	body.SwarmPool.EntryPoint = "ipfs"

	test := getSetup(t, "POST", "/networks", body)
	test.msp.RetString = "default-network"
	test.run()
	test.expectMspMethodCall(`GetDefaultNetworkName()`)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.SwarmPoolBadBodyErrorCode + `":"unable to create default network ` + body.SwarmPool.Name + ` : there is already one named ` + test.msp.RetString + `"}}`)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMspMethodCall(expectedMethod string) {
	if ts.msp.Called != expectedMethod {
		ts.t.Errorf("expected method not Called: got %v want %v",
			ts.msp.Called, expectedMethod)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMsiMethodCall(expectedMethod string) {
	if ts.msi.Called != expectedMethod {
		ts.t.Errorf("expected method not Called: got %v want %v",
			ts.msi.Called, expectedMethod)
	}
}
