package routers

import (
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/config"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type SwarmPoolModelValidator struct {
	SwarmPool struct {
		Name         string   `json:"name" binding:"required,dnsrfc1123,min=4,max=127"`
		IsPrivate    bool     `json:"is_private"`
		PrivateKey   string   `json:"private_key" binding:"required_with=JoiningNodes,isdefault_ifnotprivate,omitempty,base64,min=2,max=2048"`
		JoiningNodes []string `json:"joining_nodes" binding:"isdefault_ifnotprivate,omitempty,gte=1,lte=100,dive,max=1024"`
		EntryPoint   string   `json:"entrypoint" binding:"required,min=3,max=31"`
	} `json:"network"`
	swarmPoolModel v1alpha1.SwarmPool `json:"-"`
}

func NewSwarmPoolModelValidator() SwarmPoolModelValidator {
	return SwarmPoolModelValidator{}
}

func (s *SwarmPoolModelValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(s, binding.JSON)
	if err != nil {
		return err
	}
	s.swarmPoolModel.Namespace = s.SwarmPool.Name
	s.swarmPoolModel.Name = s.SwarmPool.Name

	swarm := v1alpha1.Swarm{}
	swarm.Name = s.SwarmPool.Name
	swarm.Network.Traefik.TraefikEntryPoint = s.SwarmPool.EntryPoint
	swarm.Network.Traefik.TraefikLoadBalancerService = config.Config.Traefik.LoadBalancerService
	swarm.Network.Traefik.TraefikLoadBalancerNamespace = config.Config.Traefik.LoadBalancerNamespace
	swarm.Network.SwarmPort = config.Config.IPFS.SwarmPort
	swarm.Network.APIPort = config.Config.IPFS.ApiPort
	swarm.Storage.StorageSize = "1Gi"
	swarm.IPFSVersion = config.Config.IPFS.Version

	if s.SwarmPool.IsPrivate {
		swarmDefinition := v1alpha1.SwarmDefinition{}
		swarmDefinition.SwarmKeySecretRef = s.SwarmPool.Name + "-secret"
		swarmDefinition.BootstrapNodeAddresses = s.SwarmPool.JoiningNodes
		swarm.SwarmDefinition = swarmDefinition
	}

	s.swarmPoolModel.Spec.Swarms = append(s.swarmPoolModel.Spec.Swarms, swarm)
	return nil
}
