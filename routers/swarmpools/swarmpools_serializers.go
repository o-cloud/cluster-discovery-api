package routers

import (
	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"

	"github.com/gin-gonic/gin"
)

type SwarmPoolResponse struct {
	Name         string   `json:"name" binding:"required"`
	IsPrivate    bool     `json:"is_private"`
	JoiningNodes []string `json:"joining_nodes,omitempty"`
	EntryPoint   string   `json:"entrypoint" binding:"required"`
}

type SwarmPoolsSerializer struct {
	C          *gin.Context
	SwarmPools []v1alpha1.SwarmPool
}

type SwarmPoolSerializer struct {
	C         *gin.Context
	SwarmPool v1alpha1.SwarmPool
}

type SwarmPoolConnectionInfoResponse struct {
	NetworkName string   `json:"network_name" binding:"required"`
	PrivateKey  string   `json:"private_key"`
	PeerID      []string `json:"peer_id"`
}

type SwarmPoolConnectionInfoSerializer struct {
	C          *gin.Context
	SwarmPool  v1alpha1.SwarmPool
	PrivateKey string
}

type SwarmPoolStatusResponse struct {
	NetworkState        api.NetworkState `json:"state"`
	IsIdentityPublished bool             `json:"is_identity_published"`
}

type SwarmPoolStatusSerializer struct {
	C                   *gin.Context
	NetworkState        api.NetworkState
	IsIdentityPublished bool
}

func (s *SwarmPoolsSerializer) Response() []SwarmPoolResponse {
	response := []SwarmPoolResponse{}
	for _, swarmpool := range s.SwarmPools {
		serializer := SwarmPoolSerializer{s.C, swarmpool}
		response = append(response, serializer.Response())
	}
	return response
}

func (s *SwarmPoolSerializer) Response() SwarmPoolResponse {
	response := SwarmPoolResponse{
		Name:         s.SwarmPool.Name,
		IsPrivate:    s.SwarmPool.Spec.Swarms[0].SwarmDefinition.SwarmKeySecretRef != "",
		JoiningNodes: s.SwarmPool.Spec.Swarms[0].SwarmDefinition.BootstrapNodeAddresses,
		EntryPoint:   s.SwarmPool.Spec.Swarms[0].Network.Traefik.TraefikEntryPoint,
	}
	return response
}

func (s *SwarmPoolConnectionInfoSerializer) Response() SwarmPoolConnectionInfoResponse {
	response := SwarmPoolConnectionInfoResponse{
		NetworkName: s.SwarmPool.Name,
		PrivateKey:  s.PrivateKey,
		PeerID:      s.SwarmPool.Status.SwarmsStatus[s.SwarmPool.Name].Addresses,
	}
	return response
}

func (s *SwarmPoolStatusSerializer) Response() SwarmPoolStatusResponse {
	response := SwarmPoolStatusResponse{
		NetworkState:        s.NetworkState,
		IsIdentityPublished: s.IsIdentityPublished,
	}
	return response
}
