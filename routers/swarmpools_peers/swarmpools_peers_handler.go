package routers

import (
	"fmt"
	"net/http"

	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/models"
	peers "gitlab.com/o-cloud/cluster-discovery-api/routers/peers"

	"github.com/gin-gonic/gin"
)

type SwarmPoolsPeersHandler struct {
	SP models.IPeers
}

func NewSwarmPoolsPeersHandler() *SwarmPoolsPeersHandler {
	return &SwarmPoolsPeersHandler{
		SP: models.GetPeers(),
	}
}

func (SP *SwarmPoolsPeersHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("/:peer", SP.handleGet)
	router.GET("", SP.handleList)
}

// Return the requested Peer in requested SwarmPool
func (SP *SwarmPoolsPeersHandler) handleGet(c *gin.Context) {
	network := c.Param("network")
	name := c.Param("peer")
	peer, err := SP.SP.GetPeer(network, name)
	if err != nil {
		body := common.NewError(common.PeerNotFoundErrorCode, fmt.Errorf("peer %s not found in swarmpool %s", name, network))
		c.JSON(http.StatusNotFound, body)
		return
	}
	serializer := peers.PeerSerializer{C: c, Peer: *peer}
	c.JSON(http.StatusOK, gin.H{"peer": serializer.Response()})
}

// Return all the peers in requested SwarmPool
func (SP *SwarmPoolsPeersHandler) handleList(c *gin.Context) {
	network := c.Param("network")
	peers_list := SP.SP.ListPeers(network)
	serializer := peers.PeersSerializer{C: c, Peers: *peers_list}
	c.JSON(http.StatusOK, serializer.Response())
}
