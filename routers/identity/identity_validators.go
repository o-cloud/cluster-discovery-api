package routers

import (
	"net/http"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/models"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type IdentityModelValidator struct {
	S        models.ISwarmPools `json:"-"`
	Identity struct {
		Name        string   `json:"name" binding:"required,dnsrfc1123,min=4,max=127"`
		URL         string   `json:"url" binding:"required,max=253"`
		Description string   `json:"description" binding:"max=253"`
		Categories  []string `json:"categories" binding:"lte=50,dive,max=63"`
	} `json:"identity"`
	identityModel v1alpha1.Identity `json:"-"`
}

func NewIdentityModelValidator(S *models.ISwarmPools) IdentityModelValidator {
	return IdentityModelValidator{S: *S}
}

func NewIdentityModelValidatorFillWith(S *models.ISwarmPools, identityModel v1alpha1.Identity) IdentityModelValidator {
	identityModelValidator := NewIdentityModelValidator(S)
	identityModelValidator.Identity.Name = identityModel.Spec.Name
	identityModelValidator.Identity.URL = identityModel.Spec.URL
	identityModelValidator.Identity.Description = identityModel.Spec.Description
	categories := make([]string, 0)
	for _, category := range identityModel.Spec.Categories {
		categories = append(categories, category.Name)
	}
	identityModelValidator.Identity.Categories = categories
	identityModelValidator.identityModel = identityModel
	return identityModelValidator
}

func (i *IdentityModelValidator) Bind(c *gin.Context) error {

	err := c.ShouldBindWith(i, binding.JSON)
	if err != nil {
		return err
	}
	i.identityModel.Spec.URL = i.Identity.URL
	i.identityModel.Spec.Name = i.Identity.Name
	i.identityModel.Spec.Description = i.Identity.Description

	if http.MethodPost == c.Request.Method {
		i.identityModel.Spec.PublicationTargets = v1alpha1.TargetedSwarms{}
		swarmpools := i.S.ListSwarmPools()
		for _, swarmpool := range *swarmpools {
			swarm := v1alpha1.TargetedSwarm{}
			swarm.Namespace = swarmpool.Namespace
			swarm.SwarmPoolName = swarmpool.Name
			swarm.Swarm = swarmpool.Name

			i.identityModel.Spec.PublicationTargets = append(i.identityModel.Spec.PublicationTargets, swarm)
		}
	}

	categories := v1alpha1.Categories{}
	for _, categoryName := range i.Identity.Categories {
		category := v1alpha1.Category{}
		category.Name = categoryName
		categories = append(categories, category)
	}
	i.identityModel.Spec.Categories = categories

	return nil
}
