package routers

import (
	"errors"
	"net/http"

	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type IdentityHandler struct {
	I models.IIdentity
	S models.ISwarmPools
}

func NewIdentityHandler() *IdentityHandler {
	return &IdentityHandler{
		I: models.GetIdentity(),
		S: models.GetSwarmPools(),
	}
}

func (I *IdentityHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", I.handleGet)
	router.POST("", I.handleCreate)
	router.PUT("", I.handleUpdate)
	router.DELETE("", I.handleDelete)
}

// Return the Identity
func (I *IdentityHandler) handleGet(c *gin.Context) {
	identity, err := I.I.GetIdentity()
	if err != nil {
		body := common.NewError(common.IdentityNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}
	serializer := IdentitySerializer{C: c, Identity: *identity}
	c.JSON(http.StatusOK, gin.H{"identity": serializer.Response()})
}

// Create the Identity
func (I *IdentityHandler) handleCreate(c *gin.Context) {
	identityModelValidator := NewIdentityModelValidator(&I.S)
	err := identityModelValidator.Bind(c)
	if err != nil {
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			body := common.NewError(common.IdentityBadRequestErrorCode, errors.New("bad request : unable to parse json body"))
			c.JSON(http.StatusBadRequest, body)
			return
		}
		body := common.NewValidatorError(common.IdentityBadBodyErrorCode, errs)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	identity, err := I.I.CreateIdentity(&identityModelValidator.identityModel)
	if err != nil {
		body := common.NewError(common.IdentityAlreadyExistErrorCode, err)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}
	serializer := IdentitySerializer{C: c, Identity: *identity}
	c.JSON(http.StatusCreated, gin.H{"identity": serializer.Response()})
}

// Update the Identity
func (I *IdentityHandler) handleUpdate(c *gin.Context) {
	previouIdentity, err := I.I.GetIdentity()
	if err != nil {
		body := common.NewError(common.IdentityNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}

	identityModelValidator := NewIdentityModelValidatorFillWith(&I.S, *previouIdentity)
	err = identityModelValidator.Bind(c)
	if err != nil {
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			body := common.NewError(common.IdentityBadRequestErrorCode, errors.New("bad request : unable to parse json body"))
			c.JSON(http.StatusBadRequest, body)
			return
		}
		body := common.NewValidatorError(common.IdentityBadBodyErrorCode, errs)
		c.JSON(http.StatusUnprocessableEntity, body)
		return
	}

	identity, err := I.I.UpdateIdentity(&identityModelValidator.identityModel)
	if err != nil {
		body := common.NewError(common.IdentityNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}
	serializer := IdentitySerializer{C: c, Identity: *identity}
	c.JSON(http.StatusOK, gin.H{"identity": serializer.Response()})
}

// Delete the Identity
func (I *IdentityHandler) handleDelete(c *gin.Context) {
	err := I.I.DeleteIdentity()
	if err != nil {
		body := common.NewError(common.IdentityNotFoundErrorCode, err)
		c.JSON(http.StatusNotFound, body)
		return
	}
	c.Status(http.StatusNoContent)
}
