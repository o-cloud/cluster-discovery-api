package routers

import (
	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"

	"github.com/gin-gonic/gin"
)

type IdentitySerializer struct {
	C        *gin.Context
	Identity v1alpha1.Identity
}

func (s *IdentitySerializer) Response() api.Identity {
	categories := []string{}
	for _, category := range s.Identity.Spec.Categories {
		categories = append(categories, category.Name)
	}
	networks := []string{}
	for _, network := range s.Identity.Spec.PublicationTargets {
		networks = append(networks, network.SwarmPoolName)
	}
	response := api.Identity{
		Name:        s.Identity.Spec.Name,
		URL:         s.Identity.Spec.URL,
		Description: s.Identity.Spec.Description,
		Categories:  categories,
		Networks:    networks,
	}
	return response
}
