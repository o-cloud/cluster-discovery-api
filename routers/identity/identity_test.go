package routers_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	"gitlab.com/o-cloud/cluster-discovery-api/common"
	routers "gitlab.com/o-cloud/cluster-discovery-api/routers/identity"
	"gitlab.com/o-cloud/cluster-discovery-api/tests/mocks"

	"github.com/gin-gonic/gin"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	msi    *mocks.MockIdentity
	msp    *mocks.MockSwarmPools
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.Default()
	msi := &mocks.MockIdentity{}
	msp := &mocks.MockSwarmPools{}
	(&routers.IdentityHandler{S: msp, I: msi}).SetupRoutes(r.Group("/identity"))
	return testSetup{
		t:      t,
		engine: r,
		msi:    msi,
		msp:    msp,
		req:    req,
		rr:     rr,
	}
}

func TestHandleGetIdentity(t *testing.T) {
	categories := make([]v1alpha1.Category, 2)
	categories[0] = v1alpha1.Category{Name: "satellite"}
	categories[1] = v1alpha1.Category{Name: "environnement"}
	publicationTargets := make([]v1alpha1.TargetedSwarm, 2)
	publicationTargets[0].SwarmPoolName = "test-network"
	publicationTargets[1].SwarmPoolName = "test-network2"
	identity := v1alpha1.Identity{
		ObjectMeta: metav1.ObjectMeta{Name: "cluster-identity", Namespace: "current-namespace"},
		Spec: v1alpha1.IdentitySpec{
			PublicationTargets: publicationTargets,
			URL:                "https://www.google.com",
			Description:        "description",
			Categories:         categories,
			Name:               "test-identity",
		},
	}

	test := getSetup(t, "GET", "/identity", nil)
	test.msi.RetArray = []v1alpha1.Identity{identity}
	test.run()
	test.expectMsiMethodCall(`GetIdentity()`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"identity":{"name":"test-identity","url":"https://www.google.com","description":"description","categories":["satellite","environnement"],"networks":["test-network","test-network2"]}}`)
}

func TestHandleGetIdentityNotFound(t *testing.T) {
	test := getSetup(t, "GET", "/identity", nil)
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msi.Err = errors.New("erreur")
	test.run()
	test.expectMsiMethodCall(`GetIdentity()`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.IdentityNotFoundErrorCode + `":"` + test.msi.Err.Error() + `"}}`)
}

func TestHandleDeleteIdentity(t *testing.T) {
	categories := make([]v1alpha1.Category, 2)
	categories[0] = v1alpha1.Category{Name: "satellite"}
	categories[1] = v1alpha1.Category{Name: "environnement"}
	publicationTargets := make([]v1alpha1.TargetedSwarm, 1)
	publicationTargets[0].SwarmPoolName = "test-network"
	identity := v1alpha1.Identity{
		ObjectMeta: metav1.ObjectMeta{Name: "cluster-identity", Namespace: "current-namespace"},
		Spec: v1alpha1.IdentitySpec{
			PublicationTargets: publicationTargets,
			URL:                "https://www.google.com",
			Description:        "description",
			Categories:         categories,
			Name:               "test-identity",
		},
	}

	test := getSetup(t, "DELETE", "/identity", nil)
	test.msi.RetArray = []v1alpha1.Identity{identity}
	test.run()
	test.expectMsiMethodCall(`DeleteIdentity()`)
	test.expectResponseStatus(http.StatusNoContent)
	test.expectBodyContent(``)
}

func TestHandleDeleteIdentityNotFound(t *testing.T) {
	test := getSetup(t, "DELETE", "/identity", nil)
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msi.Err = errors.New("erreur")
	test.run()
	test.expectMsiMethodCall(`DeleteIdentity()`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.IdentityNotFoundErrorCode + `":"` + test.msi.Err.Error() + `"}}`)
}

func TestHandleCreateIdentity(t *testing.T) {
	body_categories := make([]string, 2)
	body_categories[0] = "satellite"
	body_categories[1] = "environnement"
	body := routers.IdentityModelValidator{}
	body.Identity.Name = "test-identity"
	body.Identity.URL = "https://www.google.com"
	body.Identity.Description = "description"
	body.Identity.Categories = body_categories

	categories := make([]v1alpha1.Category, 2)
	categories[0] = v1alpha1.Category{Name: body_categories[0]}
	categories[1] = v1alpha1.Category{Name: body_categories[1]}
	publicationTargets := make([]v1alpha1.TargetedSwarm, 1)
	publicationTargets[0].SwarmPoolName = "test-network"
	identity := v1alpha1.Identity{
		ObjectMeta: metav1.ObjectMeta{Name: body.Identity.Name, Namespace: "current-namespace"},
		Spec: v1alpha1.IdentitySpec{
			PublicationTargets: publicationTargets,
			URL:                body.Identity.URL,
			Description:        body.Identity.Description,
			Categories:         categories,
			Name:               body.Identity.Name,
		},
	}

	test := getSetup(t, "POST", "/identity", body)
	test.msi.RetArray = []v1alpha1.Identity{identity}
	test.msp.RetArray = []v1alpha1.SwarmPool{}

	test.run()
	test.expectMsiMethodCall(`CreateIdentity(` + identity.Name + `)`)
	test.expectMssMethodCall(`ListSwarmPools()`)
	test.expectResponseStatus(http.StatusCreated)
	test.expectBodyContent(`{"identity":{"name":"` + identity.Name + `","url":"https://www.google.com","description":"description","categories":["satellite","environnement"],"networks":["test-network"]}}`)
}

func TestHandleCreateIdentityBadRequest(t *testing.T) {
	test := getSetup(t, "POST", "/identity", "plop")
	test.run()
	test.expectMsiMethodCall(``)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent(`{"errors":{"` + common.IdentityBadRequestErrorCode + `":"bad request : unable to parse json body"}}`)
}

func TestHandleCreateIdentityErrorValidation(t *testing.T) {
	test := getSetup(t, "POST", "/identity", v1alpha1.Identity{})
	test.run()
	test.expectMsiMethodCall(``)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.IdentityBadBodyErrorCode + `":{"Name":"required: Name is a required field","URL":"required: URL is a required field"}}}`)
}

func TestHandleCreateIdentityAlreadyExist(t *testing.T) {
	body_categories := make([]string, 2)
	body_categories[0] = "satellite"
	body_categories[1] = "environnement"
	body := routers.IdentityModelValidator{}
	body.Identity.Name = "test-identity"
	body.Identity.URL = "https://www.google.com"
	body.Identity.Description = "description"
	body.Identity.Categories = body_categories

	test := getSetup(t, "POST", "/identity", body)
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msi.Err = errors.New("Erreur")
	test.run()
	test.expectMsiMethodCall(`CreateIdentity(` + body.Identity.Name + `)`)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.IdentityAlreadyExistErrorCode + `":"` + test.msi.Err.Error() + `"}}`)
}

func TestHandleUpdateIdentity(t *testing.T) {
	body_categories := make([]string, 2)
	body_categories[0] = "satellite"
	body_categories[1] = "environnement"
	body := routers.IdentityModelValidator{}
	body.Identity.Name = "test-identity"
	body.Identity.URL = "https://www.google.com"
	body.Identity.Description = "description"
	body.Identity.Categories = body_categories

	categories := make([]v1alpha1.Category, 2)
	categories[0] = v1alpha1.Category{Name: body_categories[0]}
	categories[1] = v1alpha1.Category{Name: body_categories[1]}
	publicationTargets := make([]v1alpha1.TargetedSwarm, 1)
	publicationTargets[0].SwarmPoolName = "test-network"
	identity := v1alpha1.Identity{
		ObjectMeta: metav1.ObjectMeta{Name: body.Identity.Name, Namespace: "current-namespace"},
		Spec: v1alpha1.IdentitySpec{
			PublicationTargets: publicationTargets,
			URL:                body.Identity.URL,
			Description:        body.Identity.Description,
			Categories:         categories,
			Name:               body.Identity.Name,
		},
	}

	test := getSetup(t, "PUT", "/identity", body)
	test.msi.RetArray = []v1alpha1.Identity{identity}

	test.run()
	test.expectMsiMethodCall(`GetIdentity()UpdateIdentity(` + identity.Name + `)`)
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"identity":{"name":"` + identity.Name + `","url":"https://www.google.com","description":"description","categories":["satellite","environnement"],"networks":["test-network"]}}`)
}

func TestHandleUpdateIdentityBadRequest(t *testing.T) {
	test := getSetup(t, "PUT", "/identity", "plop")
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.run()
	test.expectMsiMethodCall(`GetIdentity()`)
	test.expectResponseStatus(http.StatusBadRequest)
	test.expectBodyContent(`{"errors":{"` + common.IdentityBadRequestErrorCode + `":"bad request : unable to parse json body"}}`)
}

func TestHandleUpdateIdentityErrorValidation(t *testing.T) {
	test := getSetup(t, "PUT", "/identity", v1alpha1.Identity{})
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.run()
	test.expectMsiMethodCall(`GetIdentity()`)
	test.expectResponseStatus(http.StatusUnprocessableEntity)
	test.expectBodyContent(`{"errors":{"` + common.IdentityBadBodyErrorCode + `":{"Name":"required: Name is a required field","URL":"required: URL is a required field"}}}`)
}

func TestHandleUpdateIdentityNotFound(t *testing.T) {
	body_categories := make([]string, 2)
	body_categories[0] = "satellite"
	body_categories[1] = "environnement"
	body := routers.IdentityModelValidator{}
	body.Identity.Name = "test-identity"
	body.Identity.URL = "https://www.google.com"
	body.Identity.Description = "description"
	body.Identity.Categories = body_categories

	test := getSetup(t, "PUT", "/identity", body)
	test.msi.RetArray = make([]v1alpha1.Identity, 1)
	test.msi.Err = errors.New("Erreur")
	test.run()
	test.expectMsiMethodCall(`GetIdentity()`)
	test.expectResponseStatus(http.StatusNotFound)
	test.expectBodyContent(`{"errors":{"` + common.IdentityNotFoundErrorCode + `":"` + test.msi.Err.Error() + `"}}`)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMsiMethodCall(expectedMethod string) {
	if ts.msi.Called != expectedMethod {
		ts.t.Errorf("expected method not Called: got %v want %v",
			ts.msi.Called, expectedMethod)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMssMethodCall(expectedMethod string) {
	if ts.msp.Called != expectedMethod {
		ts.t.Errorf("expected method not Called: got %v want %v",
			ts.msp.Called, expectedMethod)
	}
}
