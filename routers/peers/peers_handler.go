package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/cluster-discovery-api/models"
)

type PeersHandler struct {
	P models.IPeers
}

func NewPeersHandler() *PeersHandler {
	return &PeersHandler{
		P: models.GetPeers(),
	}
}

func (P *PeersHandler) SetupRoutes(router *gin.RouterGroup) {
	router.GET("", P.handleList)
}

// Return all the Peers
func (P *PeersHandler) handleList(c *gin.Context) {
	peers := P.P.ListPeers("")
	serializer := PeersSerializer{c, *peers}
	c.JSON(http.StatusOK, serializer.Response())
}
