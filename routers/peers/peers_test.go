package routers_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"
	routers "gitlab.com/o-cloud/cluster-discovery-api/routers/peers"
	"gitlab.com/o-cloud/cluster-discovery-api/tests/mocks"

	"github.com/gin-gonic/gin"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	mpeers *mocks.MockPeers
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.Default()
	mpeers := &mocks.MockPeers{}
	(&routers.PeersHandler{P: mpeers}).SetupRoutes(r.Group("/peers"))
	return testSetup{
		t:      t,
		engine: r,
		mpeers: mpeers,
		req:    req,
		rr:     rr,
	}
}

func TestHandleListPeers(t *testing.T) {
	test := getSetup(t, "GET", "/peers", nil)
	test.mpeers.RetArray = make([]v1alpha1.Peer, 2)

	categories := make([]v1alpha1.Category, 2)
	categories[0] = v1alpha1.Category{Name: "satellite"}
	categories[1] = v1alpha1.Category{Name: "environnement"}
	test.mpeers.RetArray[0] = v1alpha1.Peer{
		ObjectMeta: metav1.ObjectMeta{Name: "test-peer-1-id", Namespace: "test-network"},
		Spec:       v1alpha1.PeerSpec{URL: "https://www.google.com", Description: "description", Categories: categories, Name: "test-peer-1"},
		Status:     v1alpha1.PeerStatus{PeerStatus: "Online"},
	}
	test.mpeers.RetArray[1] = v1alpha1.Peer{
		ObjectMeta: metav1.ObjectMeta{Name: "test-peer-2-id", Namespace: "test-network"},
		Spec:       v1alpha1.PeerSpec{URL: "https://www.google.com", Description: "description", Categories: categories, Name: "test-peer-2"},
		Status:     v1alpha1.PeerStatus{PeerStatus: "Online"},
	}

	test.run()
	test.expectMethodCall("ListPeers()")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"peers":[{"network":"test-network","id":"test-peer-1-id","name":"test-peer-1","url":"https://www.google.com","description":"description","categories":["satellite","environnement"],"status":"Online"},{"network":"test-network","id":"test-peer-2-id","name":"test-peer-2","url":"https://www.google.com","description":"description","categories":["satellite","environnement"],"status":"Online"}],"peers_count":2}`)
}

func TestHandleListPeersEmpty(t *testing.T) {
	test := getSetup(t, "GET", "/peers", nil)
	test.run()
	test.expectMethodCall("ListPeers()")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"peers":[],"peers_count":0}`)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMethodCall(expectedMethod string) {
	if ts.mpeers.Called != expectedMethod {
		ts.t.Errorf("expected method not Called: got %v want %v",
			ts.mpeers.Called, expectedMethod)
	}
}
