package routers

import (
	"gitlab.com/o-cloud/cluster-discovery-api/api"
	"gitlab.com/o-cloud/cluster-discovery-api/api/v1alpha1"

	"github.com/gin-gonic/gin"
)

type PeersSerializer struct {
	C     *gin.Context
	Peers []v1alpha1.Peer
}

type PeerSerializer struct {
	C    *gin.Context
	Peer v1alpha1.Peer
}

func (s *PeersSerializer) Response() api.PeersResponse {
	response := []api.Peer{}
	for _, peer := range s.Peers {
		serializer := PeerSerializer{s.C, peer}
		response = append(response, serializer.Response())
	}
	return api.PeersResponse{Peers: response, PeersCount: len(response)}
}

func (s *PeerSerializer) Response() api.Peer {
	categories := []string{}
	for _, category := range s.Peer.Spec.Categories {
		categories = append(categories, category.Name)
	}
	response := api.Peer{
		Network:     s.Peer.Namespace,
		Id:          s.Peer.Name,
		Name:        s.Peer.Spec.Name,
		URL:         s.Peer.Spec.URL,
		Description: s.Peer.Spec.Description,
		Categories:  categories,
		Status:      string(s.Peer.Status.PeerStatus),
	}
	return response
}
