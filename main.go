package main

import (
	"log"

	"gitlab.com/o-cloud/cluster-discovery-api/common"
	"gitlab.com/o-cloud/cluster-discovery-api/config"
	identity "gitlab.com/o-cloud/cluster-discovery-api/routers/identity"
	peers "gitlab.com/o-cloud/cluster-discovery-api/routers/peers"
	swarmpools "gitlab.com/o-cloud/cluster-discovery-api/routers/swarmpools"
	swarmpools_peers "gitlab.com/o-cloud/cluster-discovery-api/routers/swarmpools_peers"

	"github.com/gin-gonic/gin"
)

func main() {
	common.LoadKubeConfig()
	config.Load()
	r := setupRouter(config.Config.Server.ApiPrefix)

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter(apiPrefix string) *gin.Engine {
	r := gin.Default()

	prefixgroup := r.Group(apiPrefix)

	identity.NewIdentityHandler().SetupRoutes(prefixgroup.Group("/identity"))
	peers.NewPeersHandler().SetupRoutes(prefixgroup.Group("/peers"))
	swarmpools.NewSwarmPoolsHandler().SetupRoutes(prefixgroup.Group("/networks"))
	swarmpools_peers.NewSwarmPoolsPeersHandler().SetupRoutes(prefixgroup.Group("/networks/:network/peers"))
	return r
}
